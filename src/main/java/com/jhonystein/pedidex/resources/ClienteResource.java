package com.jhonystein.pedidex.resources;

import com.jhonystein.pedidex.models.Cliente;
import com.jhonystein.pedidex.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("clientes")
public class ClienteResource {
    
    @Autowired
    private ClienteService service;
    
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Iterable<Cliente> findAll() {
        return service.findAll();
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente insert(@RequestBody Cliente cliente) {
        return service.save(cliente);
    }
    
    @GetMapping("{id}")
    public Cliente findById(@PathVariable("id") Long id) {
        return service.findById(id);
    }
    
    @PutMapping("{id}")
    public ResponseEntity<Cliente> update(@PathVariable("id") Long id, @RequestBody Cliente cliente) {
        if (!id.equals(cliente.getId()))
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(service.save(cliente));
    }
    
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        service.remove(id);
    }
    
    @GetMapping("busca")
    public Iterable<Cliente> findByNome(@RequestParam("nome") String nome) {
        return service.findByNome(nome);
    }
}
