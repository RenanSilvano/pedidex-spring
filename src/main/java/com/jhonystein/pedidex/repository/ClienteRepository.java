package com.jhonystein.pedidex.repository;

import com.jhonystein.pedidex.models.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long> {
    Iterable<Cliente> findByNome(@Param("nome") String nome);
}
