package com.jhonystein.pedidex.services;

import com.jhonystein.pedidex.models.Regra;
import com.jhonystein.pedidex.models.Usuario;
import com.jhonystein.pedidex.repository.RegraRepository;
import com.jhonystein.pedidex.repository.UsuarioRepository;
import com.jhonystein.pedidex.security.JwtTokenUtil;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private RegraRepository regraRepository;

    public Usuario register(Usuario usuario) {
        Regra regraUsuario = regraRepository.findByRegra("USER");

        if (usuario == null) {
            regraUsuario = new Regra();
            regraUsuario.setRegra("USER");
            regraRepository.save(regraUsuario);
        }
        usuario.setRegras(new HashSet<>(
                Arrays.asList(regraUsuario)));
        usuario.setSenha(
                passwordEncoder.encode(usuario.getSenha())
        );
        Usuario usuarioBase = usuarioRepository.save(usuario);
        usuarioBase.setToken(jwtTokenUtil.generateToken(usuarioBase));
        return usuarioBase;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return usuarioRepository.findByEmail(username);
    }

    public Usuario login(Usuario usuario) {
        authenticate(usuario.getEmail(), usuario.getSenha());
        
        Usuario usuarioBase = usuarioRepository.findByEmail(usuario.getEmail());
        usuarioBase.setToken(jwtTokenUtil.generateToken(usuarioBase));
        return usuarioBase;
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

}
