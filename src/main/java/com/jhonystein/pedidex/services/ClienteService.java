package com.jhonystein.pedidex.services;

import com.jhonystein.pedidex.models.Cliente;
import com.jhonystein.pedidex.models.Usuario;
import com.jhonystein.pedidex.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service 
public class ClienteService {
    
    @Autowired
    private ClienteRepository repository;
    
    public Iterable<Cliente> findAll() {
        return repository.findAll();
    }
    
    public Cliente save(Cliente cliente) {
        Usuario usuario = (Usuario) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        cliente.setSalvo(usuario);
        return repository.save(cliente);
    }
    
    public Cliente findById(Long id) {
        return repository.findById(id).get();
    }
    
    public void remove(Long id) {
        repository.deleteById(id);
    }
    
    public Iterable<Cliente> findByNome(String nome) {
        return repository.findByNome(nome);
    }
}
